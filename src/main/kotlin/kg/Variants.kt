package kg

import java.io.*
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class GenotypeAttributes(val sample: String, val genotypeInfos: Map<String,String>)

class Variant(
    val chr: String,
    val pos : Int,
    val ref: String,
    val alts: List<String>,
    val qual : Double,
    val genotypeInfos: List<Map<String,String>>,
    val samples : List<String>) {

    companion object {
        fun parse(variants: Variants, fields : List<String>, format: List<String>) : Variant {
            val alts = fields[4].split(',')
            val genotypeInfos = fields.drop(9).map {
                format.zip(it.split(':')).toMap()
            }
            return Variant(fields[0], fields[1].toInt(), fields[3], alts, fields[5].toDouble(), genotypeInfos, variants.samples)
        }
    }

    /**
     * Information about a variant for a specific sample
     *
     * @param   index   the index of the sample in the samples list
     * @param   sample  the name of the sample
     */
    inner class Genotype(val index: Int) {

        val alleles = genotypeInfos[index]["GT"]!!.split('/','|').map { if(it == ".") 0 else it.toInt() }

        /**
         * Generic access to the genotype fields as a Map
         * <p>
         * Fields are not parsed, so using this method requires you to parse them yourself.
         */
        val info = genotypeInfos[index]

        /**
         * Total depth of reads crossing the position
         * <p>
         * Note that this field is not guaranteed to be avaialble in a VCF file, so
         * you must handle null
         */
        val dp get() = info["DP"]?.toInt()

        /**
         * The number of copies of each allele in the allele list for this variant
         */
        val dosages = alts.mapIndexed { altIndex, alt ->
            alleles.count { (altIndex+1) == it }
        }

        val altDepth get()  : Int {
            return alleleDepths[index+1]
        }

        val altFraction get() : Double {
            val ads = alleleDepths
            val total = ads.sum()
            if(total == 0)
                return 0.0
            else
                return altDepth.toDouble() / total
        }

        val het get() : Boolean {
            return dosages[0] > 0 && dosages[0] < 2
        }

        val hom get() : Boolean {
            return dosages[0]  == 2
        }

        val homRef get() = dosages[0] == 2 && dosages.drop(1).all { it == 0 }
    }

    /**
     * Cached internal copy of genotypes
     */
    lateinit var _genotypes : List<Genotype>

    /**
     * List of genotypes, one for each sample in the VCF
     * <p>
     * Computed lazily and cached internally
     */
    val genotypes : List<Genotype> get() {
        if(!this::_genotypes.isInitialized)
            this._genotypes = samples.mapIndexed { i, sample -> Genotype(i) }
        return this._genotypes
    }

    lateinit var ads : List<Int>

    val alleleDepths get() : List<Int> {
        if(!this::ads.isInitialized)
            ads = genotypeInfos[0]["AD"]!!.split(',').map { it.toInt() }
        return this.ads
    }

    val genotype get() : Genotype = genotypes[0]

    val het get() : Boolean = this.genotypes[0].het

    val hom get() : Boolean = this.genotypes[0].hom

    val homRef get() : Boolean = this.genotypes[0].homRef

    val dosages get() = this.genotypes.map  { it.dosages[0] }

    /**
     * Return the number of copies of the first alternate allele represented by this variant for the given sample
     */
    fun dosageOf(sampleIndex : Int) : Int {
        return dosages[sampleIndex]
    }

    /**
     * Return the number of copies of the first alternate allele represented by this variant for the given sample
     */
    fun dosageOf(sample : String) : Int {
        return dosages[this.samples.indexOf(sample)]
    }
}

class Variants : Iterable<Variants.Site> {

    inner class Site(val pos : Int, val ref: String, val format: List<String>) {

        constructor(variant: Variant, format: List<String>) : this(variant.pos, variant.ref, format) {
        }

        fun add(variant: Variant) {
            assert(this.pos == variant.pos)
            assert(this.ref == variant.ref)
            this.variants.add(variant)
        }

        val variants : MutableList<Variant> = ArrayList<Variant>()

        val alt get() : String { return variants[0].alts[0] }

        val size get() : Int  { return variants.size }

        fun dosageOf(sample : String) : Int {
//            return variants[0].dosageOf(sample)
            return variants[0].dosageOf(samples.indexOf(sample))
        }
    }

    var reader : BufferedReader? = null

    var file : File? = null

    val headerLines : ArrayList<String> = ArrayList(1000)

    val sites = TreeMap<Int,Site>() // TODO: should be List<Site> ? how do VCFs with repeated site work?

    lateinit var samples : List<String>

    var defaultFormat : List<String>? = null

    val formats : MutableMap<String,List<String>> = mutableMapOf()

    constructor(source : File) {
        this.file = source
    }

    constructor(source : String) : this(File(source)) {
    }

    constructor(reader : Reader) {
        this.reader = if(reader is BufferedReader) (reader as BufferedReader) else BufferedReader(reader)
    }

    constructor(stream : InputStream) : this(InputStreamReader(stream)){
    }

    operator fun get(pos : Int) : Site? {
        return this.sites[pos]
    }

    fun parse() : Variants {
        val reader = this.reader ?: BufferedReader(FileReader(this.file))
        reader.use {
            parseReader(it)
        }
        return this
    }

    fun parseReader(reader : BufferedReader) {

        // Read the headers first
        var line = reader.readLine()
        var lastHeaderLine : String? = null
        while((line!=null) && line.startsWith("#")) {
            headerLines.add(line)
            lastHeaderLine = line
            line = reader.readLine()
        }

        this.samples = lastHeaderLine!!.split('\t').drop(9)

        while(line != null) {

            val fields = line.split('\t')
            val format = formats.computeIfAbsent(fields[8]) {
                it.split(':')
            }

            val variant = Variant.parse(this, fields, format)
            this.sites.computeIfAbsent(variant.pos) {
                this.Site(variant, format)
            }.add(variant)

            line = reader.readLine()
        }
    }

    override fun iterator(): Iterator<Site> {
        return this.sites.values.iterator()
    }

}