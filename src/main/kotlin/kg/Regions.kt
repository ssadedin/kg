package kg

import java.io.File
import org.apache.commons.collections4.iterators.IteratorChain

class Regions<T> : Iterable<Region<T>>{

    val indexes = LinkedHashMap<String,RangeIndex<T>>()
    
    companion object {
        fun bed(path : String) : Regions<String> {
            val result = Regions<String>()
            File(path).forEachLine { line ->
                val parts = line.split('\t')
                val chr = parts[0]
                val data = if(parts.size>3) parts[3] else null
                val region = Region<String>(chr, parts[1].toInt(), parts[2].toInt(), data)
                result.add(region)
            }
            return result
        }
    }
    
    constructor() {
    }
    
    constructor(regions : Iterable<Region<T>>) {
        regions.forEach { this.add(it) }
    }
    
    @Suppress("UNCHECKED_CAST")
    override fun iterator(): Iterator<Region<T>> {
        return IteratorChain(indexes.values.map { it.iterator() }) as Iterator<Region<T>>
    }
    
    fun add(region : Region<T>) : Regions<T> {
        val chr = region.chr
        val index = indexes.computeIfAbsent(chr) {
            RangeIndex(chr)
        }
        index.add(region)
        return this
    }
    
    fun reduce() : Regions<T> {
        return byContig {
            it.reduce()
        }
    }

    fun intersect(other : Regions<T>) : Regions<T> {
        return byContig { ri : RangeIndex<T> ->
            val oi = other.indexes[ri.chr]
            oi?.intersect(ri)
        }
    }
     
    fun byContig(op : ((RangeIndex<T>) -> RangeIndex<T>?)) : Regions<T> {
        val result = Regions<T>()
        indexes.forEach { e ->
            val resultIndex = op(e.value)
            resultIndex?.let {
                result.indexes[e.key] = resultIndex
            }
        }
        return result
    }
  
    val numberOfRanges : Int
        get() = this.indexes.map { it.value.numberOfRanges }.sum()
    
    val size : Int
        get() = this.map { it.size }.sum()
}