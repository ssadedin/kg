package kg

import java.util.TreeMap
import java.util.ArrayList
import java.util.Collections
import java.util.TreeSet
import java.util.Comparator
import java.util.Arrays


/**
 * Comparator that orders ranges first by their starting position
 * and then by their end position.
 */
class IntRangeComparator<T> : Comparator<Region<T>> {
    override fun compare(a: Region<T>, b: Region<T>): Int {
        if(a === b) {
            return 0
        }
        else {
            val d = a.from - b.from; 
            if(d != 0) {
                return d
            }

            val d2 = a.to - b.to 
            if(d2 == 0) {
                return System.identityHashCode(a).compareTo(System.identityHashCode(b))
            }
            else {
                return d2
            }
        }
    }
}

/**
 * Indexes a set of ranges so that the ranges crossing any point
 * can be identified quickly and easily.
 * <p>
 * The RangeIndex models ranges by their breakpoints. Each time a new range
 * is inserted, the breakpoints are stored in a tree and the list of overlapping ranges
 * at each breakpoint is tracked for each breakpoint entry. This makes it 
 * easy to find overlapping ranges at any point. This class is the primary driver
 * for the {@link BED} class which adds support for contigs so that a whole
 * reference sequence containing multiple contigs (eg: chromosomes) can be
 * indexed.
 * <p>
 * The RangeIndex is built upon Groovy's built in {@link IntRange} class. An 
 * important aspect of this class is that by default its end point is included
 * in the range, while by default BED ranges are exclusive of the end point.
 * Thus care needs to be taken in translating between the two. When you add
 * a range to the index it is treated as a BED range: that is, the end point
 * is exclusive. When you retrieve it, the end position will be <i>inclusive</i>
 * ie: it will be decremented by one from what you inserted.
 * <p>
 * RangeIndex does not de-duplicate identical ranges. If you put in multiple 
 * identical ranges they will be separate stored and separately returned as outputs
 * for queries and iteration over the index.
 * <p>
 * <b>Usage</b>
 * <p>
 * Adding ranges is done via the <code>add</code> method:
 * <pre>
 * RangeIndex index = new RangeIndex()
 * index.add(5..10)
 * index.add(20..30)
 * index.add(25..35)
 * </pre>
 * The {@link RangeIndex} class implements the {@link Iterable} interface, 
 * allowing generic Groovy collections methods to work:
 * <code>
 * index.each { println "A range from $it.from-$it.to" }
 * assert index.grep { it.from > 15 } == 2
 * </code>
 * <p>
 * Design notes: RangeIndex stores intervals as a TreeMap (a balanced tree), indexed
 * by breakpoints. Thus there is an entry in the index for every "breakpoint" (start 
 * and end) of every range in the index. The value at each breakpoint is a list of the 
 * ranges that overlap the breakpoint. For example, if a range 
 * a..b is added (inclusive of both a and b), an entry at <code>a</code> is created and
 * the list at index <code>a</code> will contain the range <code>a..b</code>. Another
 * entry at <code>b+1</code> will be created, which does not contain <code>a</code>. 
 * (If there are no other overlapping ranges, the entry at <code>b+1</code> will be
 * empty).
 * 
 * @author simon.sadedin@mcri.edu.au
 */
class RangeIndex<T>(chr : String) : Iterable<Region<T>> {
    

    val chr : String
    
    val ZERO : Int = 0
    
    val INT_RANGE_COMPARATOR = IntRangeComparator<T>()
    
    val ranges = TreeMap<Int,ArrayList<Region<T>>>()
    
    var numberOfRanges : Int = 0
    
    companion object {
        fun of(chr: String, ranges: Iterable<IntRange>) : RangeIndex<Any> {
            return RangeIndex(chr, ranges.map { r : IntRange ->
                BasicRegion(chr, r.start, r.endInclusive)
            })
        }
        
        fun of(chr : String, vararg regions: IntRange) : RangeIndex<Any> {
            return RangeIndex(chr, regions.map { r : IntRange ->
                BasicRegion(chr, r.start, r.endInclusive)
            })

        }
  
    }
    
    init {
        this.chr = chr
    }
    
    constructor(chr : String, regions: Iterable<Region<T>>) : this(chr){
        for(r in regions) this.add(r)
    }
    

    
    // Sadly causes runtime error:
    //
    // Platform declaration clash: The following declarations have the same JVM signature
    //
//    constructor(chr : String, regions: Iterable<IntRange>) : this(chr){
//        for(r in regions) {
//            this.add(Region(this.chr, r.start, r.endInclusive))
//        }
//    }
  
    fun add(newRange : Region<T>) {
        
        ++numberOfRanges
       
        // Any existing range over start position?
        val startPosition = newRange.from
        val endPosition = newRange.to

        val lowerEntry = ranges.lowerEntry(startPosition+1)
        
        // Inserting a range at the lowest end is relatively straightforward, so 
        // handle it separately
        if(lowerEntry == null || lowerEntry.value.isEmpty()) { 
            addLowestRange(newRange)
            return
        }
        
        // Already a range preceding this position: we to check for overlap and maybe split it
        if(ranges.containsKey(startPosition)) { // If starts at exactly same position, share its entry
            ranges[startPosition]!!.add(newRange)
            checkRanges(startPosition)
        }
        else { // starts at a new position, have to find overlapping ranges and split them
            splitStartRange(newRange, lowerEntry)
        }
                
        var containedEntry = ranges.higherEntry(startPosition)
        while(containedEntry != null && containedEntry.key < endPosition) {
            val higherEntry = ranges.higherEntry(containedEntry.key)   
            addContainedEntry(containedEntry, higherEntry, newRange)
            containedEntry = higherEntry
        }
       
        if(!ranges.containsKey(endPosition+1)) {
            val fullyContained = containedEntry != null && containedEntry.key >= endPosition
            if(fullyContained && lowerEntry != null) {
                ranges[endPosition+1] = ArrayList(lowerEntry.value.filter { range -> endPosition < range.to })
            }
            else {
                ranges[endPosition+1] = ArrayList()
            }
        }
    }

    fun addContainedEntry(containedEntry : Map.Entry<Int,ArrayList<Region<T>>> , higherEntry : Map.Entry<Int,ArrayList<Region<T>>>? , newRange : Region<T>) {
       
        val endPosition = newRange.to
        
       
        // Note: boundaryPoint is -1 when the ranged overlapped is an "ending" range - one at the
        // border of a gap with no overlapping ranges. In that case we need to add ourselves to the
        // boundary breakpoint and break
        // TODO: why is only the first range considered for this test?
        val boundaryPoint : Int = if(higherEntry!=null) {
            higherEntry.key
        }
        else if(containedEntry.value.isEmpty()) {
            -1
        }
        else {
            containedEntry.value[0].to
        }

        // If existing range is entirely contained within the one we are adding
        // then just add our new range to the list of ranges covered
        // 
        //   |-------------------------|  # Range to add
        //           |------|             # Other range whose boundary is inside this one
        if(endPosition > boundaryPoint) { // Entirely contained

            // If there's no higher entry at all, then we can just add a new boundary
            // and a region with only our range and break
            if(higherEntry == null) {
                if(boundaryPoint>=0) {
                    val a = ArrayList<Region<T>>()
                    a.add(newRange)
                    ranges[boundaryPoint] = a
                    // checkRanges(boundaryPoint)
                }
            }
        }
        else { // The start is contained, but the end is not : split the upper range
            
            //   |-------------------|         # Range to add
            //           |------------------|  # Other range whose boundary lies past the end of this one
            this.splitEndRange(newRange, containedEntry)
        }
        assert(containedEntry.key < endPosition)

        containedEntry.value.add(newRange)
    }
    
    fun splitEndRange(newRange : Region<T>, containedEntry : Map.Entry<Int,ArrayList<Region<T>>> ) {
        
        val endPosition = newRange.to
        val afterEndPosition = endPosition+1
        val existingEndRanges = ranges[afterEndPosition]
        
        // Make a new range from the end of our range to the start of the next higher range
        // It needs to have the previous range only, not our new range
        val clonedList = ArrayList<Region<T>>(containedEntry.value.size)
        if(existingEndRanges != null)
            clonedList.addAll(existingEndRanges)
            
        for(range : Region<T> in containedEntry.value) {
            if(endPosition < range.to )
                clonedList.add(range)
        }
        ranges[afterEndPosition] = clonedList
        checkRanges(afterEndPosition)
    }
        
    fun splitStartRange(newRange : Region<T>, lowerEntry : Map.Entry<Int,ArrayList<Region<T>>> ) {
                
            val startPosition = newRange.from
            
            // Add all the overlapping regions from the adjacent lower region to
            // our new breakpoint
            val lowerSplitRegion =  ArrayList<Region<T>>(lowerEntry.value.size)
            for(lwrRange in lowerEntry.value) {
                if(lwrRange.to > startPosition)
                    lowerSplitRegion.add(lwrRange)
            }
                
            // Add our new range as covered by the split part
            lowerSplitRegion.add(newRange)
            ranges[startPosition] = lowerSplitRegion
            checkRanges(startPosition)
    }
    
    fun addLowestRange(newRange : Region<T>) {
        
        val startPosition = newRange.from
        val endPosition = newRange.to
        val positionAfterEnd = endPosition+1
        
        ranges.put(startPosition,ArrayList(listOf(newRange)))

        // checkRanges(startPosition)
        
        // If there are any ranges that start before the end of this new range,
        // add a breakpoint, and add this range to the intervening ranges
        val entryPos = startPosition
        var higherEntry = ranges.higherEntry(startPosition)
        var lastEntry : Map.Entry<Int, ArrayList<Region<T>>>?  = null
        while(higherEntry != null && higherEntry.key <= endPosition) {
            higherEntry.value.add(newRange)
            // checkRanges(higherEntry.key)
            lastEntry = higherEntry
            higherEntry = ranges.higherEntry(higherEntry.key)
        }
        
        // The last region needs to be split into two
        if(lastEntry != null) {
            if(!ranges.containsKey(positionAfterEnd)) {
              var newRanges = ArrayList<Region<T>>(lastEntry.value.size)
              for(r in lastEntry.value) {
                  if(positionAfterEnd>=r.from && positionAfterEnd<=r.to)
                      newRanges.add(r)
              }
              ranges[positionAfterEnd] = newRanges
              // checkRanges(endPosition+1)
            }
        }
        else {
            // Need to mark the ending of the range so that future adds will
            // look up the right point for adding new overlapping ranges
            if(!ranges.containsKey(positionAfterEnd)) {
                // println "Adding ${endPosition+1} to index ..."
                ranges[positionAfterEnd] = ArrayList()
            }
        }
    }
    
 
    /**
     * Check that all the ranges added at a position span that position
     * 
     * @param position
     */
    fun checkRanges(position : Int) {
        return; // disabled!
        
        val rangesAtPosition = ranges.get(position)
        
        
        if(rangesAtPosition == null)
            return 
           
        for(r in rangesAtPosition) { 
            assert(r.from <= position && r.to>=position)
        }
    }
    
    fun getOverlaps(start : Int, end : Int, returnFirst : Boolean = false) : List<Region<T>> {
        
        val resultSet = TreeSet<Region<T>>(INT_RANGE_COMPARATOR) // TreeSet<IntRange> 
            
        var entry = ranges.lowerEntry(start+1)

        if(entry == null)
            entry = ranges.higherEntry(start)
            
        // Iterate as long as we are in the range
        while((entry != null) && entry.key <= end) {
            for(r in entry.value) {
               if(r.from<=end && r.to>=start) {
                   resultSet.add(r)
                   if(returnFirst)
                       return resultSet.map { it } 
               }
            }
            entry = ranges.higherEntry(entry.key)
        }

        return resultSet.map { it } // inefficient?
    }
    
    fun intersect(other : RangeIndex<T>) : RangeIndex<T> {
        val result = RangeIndex<T>(this.chr)
        this.forEach { r : Region<T> ->
            val ixs = other.intersect(r.from, r.to)
            for(ix in ixs) {
                result.add(ix)
            }
        }
        
        return result
    }
    
   /**
     * Return a list of ranges that intersect the given start and end points,
     * clipped to their overlapping portion.
     * 
     * <p>
     * <em>Note:</em>The start and end point are both treated as <b>inclusive</b>.
     * @param start start of range to find intersections for
     * @param end   end of range to find intersections for
     * @return  List of ranges that intersect the specified range.
     */
    fun intersect(start : Int, end : Int) : List<Region<T>>  {

        val result = getOverlaps(start,end)

        return result.map { r ->
            Region<T>(r.chr, Math.max(r.from, start), Math.min(r.to, end))
        }.filter {
            it.to >= it.from
        }
    }
    
    
    fun reduce() : RangeIndex<T> {
        return this.reduce(null)
    }

    /**
     * Merge all overlapping ranges together to make simplified regions
     * representing all the ranges covered by any range in this RangeIndex.
     * 
     * @return
     */
    fun reduce(reducer : ((r1: Region<T>,r2:Region<T>) -> T?)?) : RangeIndex<T> {
        
        val reduced = RangeIndex<T>(this.chr)
        
        // We take advantage of the fact that we iterate the ranges in order of genomic start position
        var currentRange : Region<T>? = null

        for(r in this) {

            if(null == currentRange) {
                currentRange = r
                continue
            }
            
            assert(r.from >= currentRange.from)
            
            if(r.overlaps(currentRange)) {
                var newExtra = currentRange.data
                if(newExtra == null) { // no data value in current range, so we can use the other's
                    newExtra = r.data
                }
                else
                if(reducer != null) { // Both are have data values - they need to be merged
                    newExtra = reducer(currentRange, r)
                }
                currentRange = Region(this.chr, Math.min(r.from, currentRange.from),Math.max(r.to, currentRange.to), null)
                currentRange.data = newExtra
            }
            else {
                reduced.add(currentRange)
                currentRange = r
            }
        }
        
        if(null != currentRange) {
            reduced.add(currentRange)
        }
            
        return reduced
    }
    
    override fun iterator() : Iterator<Region<T>> {
        return iteratorAt(-1)
    }

    /**
     * Return an iterator that will return each range in the index
     * in genomic order by starting position, starting from the given 
     * position.
     */
    fun iteratorAt(from : Int) : Iterator<Region<T>> {
        
        val startingPos = if(from >= 0) {
            if(ranges.containsKey(from)) from  else ranges.higherKey(from)
        }
        else {
            from
        }

        return object : Iterator<Region<T>> {
            
            // Note: pos is set to null when iteration is finished
            var pos : Int? = startingPos
            
            
            // At any given position there could be multiple ranges that start there
            // this index tracks which of the ones at a position we are up to
            var index : Int = 0

            val activeRanges : List<IntRange>  = listOf()

            var nextRange : Region<T>? = null
            
            override fun hasNext() : Boolean {
                if(nextRange != null)
                    return true
                    
                // Note: pos becomes null when iteration through index exhausted
                // nextRange is set null at each call of next()
                while(pos != null && (nextRange == null || nextRange!!.from < pos!!)) {
                  nextRange = findNext()
                }
                return nextRange != null
            }
            
            override fun next() : Region<T> {
                
                if(!hasNext()) // Note: populates nextRange
                    throw IndexOutOfBoundsException("No more ranges in this index")
                
                val result = nextRange
                nextRange = null
                return result!!
            }
            
            private fun findNext() : Region<T>? {
                if(pos == -1) {
                    if(ranges.isEmpty())
                        pos = null // finished
                    else
                        pos = ranges.firstKey()
                }
                else
                if(index > ranges[pos]!!.size-1) {
                    index = 0
                    
                    pos = ranges.higherKey(pos) // no higher key => pos = null, iteration finished
                    while(pos != null && ranges[pos!!]!!.isEmpty()) {
                      pos = ranges.higherKey(pos)
                    }
                }
                
                if(pos == null) 
                    return null
                
                return ranges[pos!!]?.get(index++)
            }
        }
    }        
}