package kg.tool

import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.*
import htsjdk.samtools.SAMRecord
import htsjdk.samtools.fastq.FastqRecord
import htsjdk.samtools.reference.IndexedFastaSequenceFile
import htsjdk.samtools.util.SequenceUtil

import kg.*
import java.io.File
import java.io.FileOutputStream
import java.io.Writer
import java.lang.Exception
import java.util.*

import java.util.logging.Logger
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PendingReadCache(val writer : Writer) {
    /**
     * Internal cache of reads waiting for their mate to be encountered
     */
    private val pendingReads = HashMap<String,FastqRecord>(20000)

    fun add(record: FastqRecord) {
        if(pendingReads.containsKey(record.readName)) {
            val r1 = pendingReads.remove(record.readName)!!

            writer.write(r1.toFastQString())
            writer.write("\n")
            writer.write(record.toFastQString())
            writer.write("\n")
        }
        else {
            pendingReads.put(record.readName, record)
        }
    }
}

class ReadDropRequest(val satisfied : Boolean = false) {

}

class Deletioner : ToolBase() {

    val bamPath  by option(help="BAM file to insert deletion into").file(exists = true).required()

    val ref by option(help="Genome reference FASTA (indexed)").file(exists=true).required()

    val deletionStart by option(help="Start position of deletion in chr:pos format").required()

    val deletionSize by option(help="End position of deletion in bp").int().restrictTo(min=1).required()

    val noCovComp by option(help="filter additional reads to balance coverge").flag()

    val fraction by option(help="Fraction of reads to simulate the deletion in").double().restrictTo(min=0.0).default(1.0)

    val noSplit by option(help="suppress output of split reads").flag()

    val out : File? by option(help="File to write FASTQ to").file(exists=false)

    private val dropList : MutableList<Region<ReadDropRequest>> = ArrayList()

    lateinit var outputReadCache : PendingReadCache

    lateinit var fasta : IndexedFastaSequenceFile

    override fun run() {


        val bam = Alignment(this.bamPath.absolutePath)

        val startPos = deletionStart.split(':')[1].toInt()

        val deletion = BasicRegion(deletionStart.split(':')[0], startPos, startPos + deletionSize)

        log.info("Inserting deletion at ${deletionStart} of size ${deletionSize}bp")

        this.fasta = IndexedFastaSequenceFile(ref)

        val outStream = if(this.out!=null) {
            FileOutputStream(out)
        } else {
            System.out
        }

        outStream.bufferedWriter().use { writer ->
            this.outputReadCache = PendingReadCache(writer)
            bam.useIterator { i ->
                injectDeletionToReads(i, deletion)
            }
        }

//        if(!this.noCovComp && !this.dropList.isEmpty()) {
            log.info("${dropList.size} reads could not be dropped to balance coverage")
//        }
    }

    private fun injectDeletionToReads(i: Iterator<SAMRecord>, deletion: BasicRegion) {

        val hashCodeThreshold = (fraction * 100).toInt()

        for (r in i) {

            val readRegion = Region(r)

            val hashCode = Math.abs(r.readName.hashCode()) % 100

            // If the read does not overlap the deletion then write it out untouched
            if (readRegion.overlaps(deletion) && hashCode < hashCodeThreshold) {
                    writeReadOverlappingDeletion(deletion, readRegion, r)
            }
            else {
                // Write out the read normally
                // But: if a read cut by a deletion was placed into this region,
                // then we should shorten the output FASTQ  by equivalent amount

                val bases = r.readBases
                if(r.readNegativeStrandFlag)
                    SequenceUtil.reverseComplement(bases)

                val fastq = FastqRecord(r.readName, r.readBases, "*", r.baseQualities)
                outputReadCache.add(fastq)
            }
        }
    }

    private fun writeReadOverlappingDeletion(
        deletion: BasicRegion,
        readRegion: Region<SAMRecord>,
        read: SAMRecord
    ) {
        // Write out the read with the bases from the portion of the reference
        // after the deletion finishes
        val bases = read.readBases!!

        val pairRegion = BasicRegion(readRegion.chr,
            Math.min(read.alignmentStart, read.mateAlignmentStart),
            Math.max(read.alignmentEnd, read.mateAlignmentStart+read.readLength)
        )

        val delPad1bp = deletion.widen(1)

        // The deletion fully contains the read: replace it with reference at equivalent distance
        if (deletion.contains(pairRegion)) {
            return
        }
        else
        if (delPad1bp.contains(readRegion)) {
            val retain = adjustBasesForContainedRead(read, deletion, readRegion, bases)
            if(!retain) {
                log.info("Dropping read ${read.readName} to balance shifted read placed into overlapping region")
                return
            }
        }
        else {

            if(this.noSplit) {
                log.info("Dropping read ${read.readName} because split reads disabled")
                return
            }

            adjustForStartOverlap(readRegion, deletion, read, bases)

            adjustForEndOverlap(readRegion, deletion, read, bases)
        }

        if(read.readNegativeStrandFlag)
            SequenceUtil.reverseComplement(bases)

        val fastq = FastqRecord(
            read.readName,
            bases,
            "*",
            read.baseQualities
        )
        outputReadCache.add(fastq)
    }

    private fun adjustForEndOverlap(
        readRegion: Region<SAMRecord>,
        deletion: BasicRegion,
        read: SAMRecord,
        bases: ByteArray
    ) {
        if (readRegion.overlaps(deletion.chr, deletion.to)) {
            //                 |---- read ----|
            //  |------  deletion ----|
            val clippedPortionSize = deletion.to - read.alignmentStart
            val newBases =
                fasta.getSubsequenceAt(
                    readRegion.chr,
                    (deletion.from - clippedPortionSize).toLong(),
                    deletion.from.toLong()
                ).bases
            newBases.copyInto(bases)
        }
    }

    private fun adjustForStartOverlap(
        readRegion: Region<SAMRecord>,
        deletion: BasicRegion,
        read: SAMRecord,
        bases: ByteArray
    ) {
        if(!readRegion.overlaps(deletion.chr, deletion.from))
            return

        // read overlaps start of deletion
        // |---- read ----|
        //        |------  deletion ----|

        val newBases =
            fasta.getSubsequenceAt(
                readRegion.chr,
                deletion.to.toLong(),
                (deletion.to + (read.alignmentEnd - deletion.from)).toLong()
            ).bases

        newBases.copyInto(bases, deletion.from - read.alignmentStart -1)
    }

    /**
     * When a read is fully contained by the deletion we replace the bases of the read
     * with all the bases that would be present if the read was read from a genome
     * with the deletion in it, if the insert size of the read fragment was maintained.
     *
     * @return true if read should be retained or false if should be dropped
     */
    private fun adjustBasesForContainedRead(
        r: SAMRecord,
        deletion: BasicRegion,
        readRegion: Region<SAMRecord>,
        bases: ByteArray
    ) : Boolean {
        // Two scenarios: mate is before the read, mate is after read
        val newReadStart = if (r.mateAlignmentStart < r.alignmentStart) {
            r.alignmentStart + deletion.size
        } else {
            r.alignmentStart - deletion.size
        }

        val newReadEnd = newReadStart + r.readLength-1

        log.info("Read ${r.readName} shifted from ${r.alignmentStart} to $newReadStart")

        val newReadRegion = Region(r.referenceName, newReadStart, newReadEnd, ReadDropRequest())

        if(dropList.any { it.overlaps(newReadRegion)}) {
            val dropRequest = dropList.maxBy { it.mutualOverlap(newReadRegion) }
            if (dropRequest != null) {
                dropList.remove(dropRequest)

                // Note: this will ensure the mate does not get written even if it was processed first, because the read
                // output cache only writes out reads if both reads get emitted
                return false
            }
        }

        val newBases =
            fasta.getSubsequenceAt(readRegion.chr, newReadStart.toLong(), newReadEnd.toLong()).bases

        if(!this.noCovComp)
            dropList.add(newReadRegion)

        newBases.copyInto(bases)

        return true
    }

    companion object {

        private val MIN_READ_OUTPUT_SIZE: Int = 50

        val log = Logger.getLogger("Deletioner")
    }
}

fun main(args: Array<String>) {
    Deletioner().main(args)
}


