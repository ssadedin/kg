package kg.tool

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.arguments.optional
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import kg.Variants
import java.io.File
import java.util.logging.Logger

class Sex : ToolBase() {

    companion object {
        val log = Logger.getLogger("Sex")
    }

    val sampleSize : Int by option(help="Number of variants to sample on X chromosome").int().default(500)

    val vcfs by argument(help="VCF files to infer sex for").file(exists = true).multiple(required = true)

    val minDP : Int by option(help="Depth threshold for filtering variants").int().default(20)

    val minQual : Int by option(help="Quality threshold for filtering variants").int().default(100)

    override fun run() {
        log.info("Determining sex for samples using at least ${sampleSize} variants: ")

        vcfs.map {
            it to inferSex(it)
        }.forEach {
            if(vcfs.size>1)
                println("${it.first}\t${it.second}")
            else
                println("${it.second}")
        }
    }

    /**
     * Infer sex for the given VCF file
     */
    private fun inferSex(vcfFile: File): String {

        log.info("Inferring sex for VCF ${vcfFile}")

        val vcf = Variants(vcfFile).parse()

        val hetCount =
            vcf.flatMap { it.variants }
                .filter { it.chr == "chrX" && (it.genotype.dp?:0 > minDP) && it.qual > minQual}
                .groupBy {
                    it.het
                }.map {
                    (if (it.key) "het" else "hom") to it.value.size
                }.toMap()


        val (hets, homs) = listOf(hetCount["het"]?:0, hetCount["hom"]?:0)

        log.info("Found ${hets} heterozygotes and ${homs} homozygotes")

        if(hets + homs < sampleSize) {
            log.warning("Unable to infer sex for VCF file $vcfFile because it has less than $sampleSize variants on the X chromosome")
            return "UNKNOWN"
        }

        if(hets==0)
            return "MALE"

        if(homs==0)
            return "FEMALE"

        if(homs > hets * 2)
            return "MALE"
        else
            return "FEMALE"
    }
}

fun main(args: Array<String>) = Sex().main(args)