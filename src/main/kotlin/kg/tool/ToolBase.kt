package kg.tool

import com.github.ajalt.clikt.core.CliktCommand
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.*
import java.util.logging.Formatter

class SimpleLogFormatter : Formatter() {

    companion object {

        val lineSep = System.getProperty("line.separator");

        val DOT : Char = '.'
    }

    /**
     * A Custom format implementation that is designed for brevity.
     */
    override fun format(record : LogRecord) : String {

        val format = SimpleDateFormat("h:mm:ss");

        var loggerName = record.getLoggerName()?:"root";

        val dotIndex = loggerName.lastIndexOf(DOT)
        if(dotIndex>=0)
            loggerName = loggerName.substring(dotIndex+1)

        val output = StringBuilder()
            .append(loggerName)
            .append("\t[")
            .append(record.threadID)
            .append("]\t")
            .append(record.getLevel()).append("\t|")
            .append(format.format(Date(record.getMillis())))
            .append(' ')
            .append(record.getMessage()).append(' ')
            .append(lineSep);

        if(record.getThrown()!=null) {
            val w = StringWriter()
            record.getThrown().printStackTrace(PrintWriter(w))
            output.append("Exception:\n" + w.toString())
        }

        return output.toString();
    }
}

open abstract class ToolBase() : CliktCommand() {

    init {
        configureSimpleLogging()
    }

    /**
     * Set up Java logging with a nice, simple, reasonable format
     */

    fun configureSimpleLogging(level : Level = Level.INFO) {
        val console = ConsoleHandler()
        console.setFormatter(SimpleLogFormatter())
        console.setLevel(level)
        val log = Logger.getLogger("dummy")
        val parentLog = log.getParent()
        parentLog.getHandlers().forEach { parentLog.removeHandler(it) }

        log.getParent().addHandler(console)

        // Disable logging from groovy sql
        Logger.getLogger("groovy.sql.Sql").useParentHandlers = false
    }
}