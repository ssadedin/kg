package kg

import htsjdk.samtools.SAMRecord

class ReadPair(val r1 : SAMRecord) : Comparable<ReadPair> {

    lateinit var r2 : SAMRecord

    val r1ReferenceIndex get() = r1.referenceIndex

    val r2ReferenceIndex get() = r1.mateReferenceIndex

    val readName get() = r1.readName

    val chimeric get() = r1.referenceIndex!=r1.mateReferenceIndex

    override fun compareTo(other: ReadPair): Int {

        var cmp = r1ReferenceIndex - other.r1ReferenceIndex
        if(cmp != 0)
            return cmp

        if(r1 != null)
            cmp = this.r1.alignmentStart - other.r1.alignmentStart

        if(cmp != 0)
            return cmp

        if(r2 == null)
            return 0
        else {

            val otherAlignmentStart: Int = other.r2?.alignmentStart ?: 0

            cmp = this.r2.alignmentStart - otherAlignmentStart
            if (cmp != 0)
                return cmp

            return this.r1.readName.compareTo(other.r1.readName)
        }
    }
}