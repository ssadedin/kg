package kg

import htsjdk.samtools.SamReader
import htsjdk.samtools.SAMRecord
import htsjdk.samtools.CigarElement
import htsjdk.samtools.CigarOperator

import htsjdk.samtools.CigarOperator.*


/**
 * Provides access to summary information about the bases of a pileup
 * at a given genomic position.
 */
class Pileup(iterator: PileupIterator) {

    val iterator = iterator
    val position = iterator.iteratorPosition
    
    companion object {
        const val A : Byte = 'A'.toByte()
        const val T : Byte = 'T'.toByte()
        const val C : Byte = 'C'.toByte()
        const val G : Byte = 'G'.toByte()
        const val D : Byte = 0
    }
    
    /**
     * Returns a summary of the coverage at the location:
     * 
     * [ A, T , C, G, Deletion, total ]
     */
    val summary : IntArray get() {

        var a : Int =0
        var c : Int =0
        var t : Int =0
        var g : Int =0
        var d : Int =0
       
        for(s in iterator.alignments) {
            when(s.base) {
            
              A -> ++a;
              T -> ++t;
              C -> ++c;
              G -> ++g;
              D -> ++d;
            }
        }
        return intArrayOf(a,t,c,g,d,a+t+c+g+d);
    }
    
    /**
     * Convenience method to return counts of each base as a map indexed
     * by the base as a string. This is not efficient, but useful in non
     * performance sensitive contexts.
     * @return  map of count indexed by base
     */
    val summaryAsMap : Map<String, Int> get()  {
        val summary = summary
        return mapOf(
            "A" to summary[0],
            "T" to summary[1],
            "C" to summary[2],
            "G" to summary[3],
            "D" to summary[4]
        )
    }
    
}

/**
 * Additional state about a read that is part of a pileup
 * 
 * @author simon.sadedin@mcri.edu.au
 */
class PileupState(read : SAMRecord) {
    
    val read = read
    
   
    var cigar : CigarOperator 
    
    val cigarIterator : Iterator<CigarElement> = read.getCigar().getCigarElements().listIterator()
    
    var cigarElement :  CigarElement

     /**
     * Offset within the current cigar element
     */
    var cigarPos : Int = -1;
    
    /**
     * Offset from start of read for the next 
     */
    var nextReadPos : Int = -1;
    
    var base : Byte = -1
    
    var qual : Byte = 0 

    var cigarLength : Int 
    
    val readBases : ByteArray = read.readBases
    
    val qualities : ByteArray = read.baseQualities
    
    init {
        if(!cigarIterator.hasNext())
            throw IllegalStateException("Bad CIGAR for read " + read.getReadName() + " has no elements");

        this.cigarElement = cigarIterator.next();
        val rawCigarLength = cigarElement.getLength();

        this.cigar = cigarElement.getOperator();
        
        // Since soft clipped reads are before the alignment
        // start position, we have to skip over them
        // to be consistent with the overall pileup
        if(cigar == CigarOperator.S) {
            this.nextReadPos = rawCigarLength
            this.cigarElement = cigarIterator.next();
            this.cigarLength = cigarElement.getLength();
            this.cigar = cigarElement.getOperator();
        }
        else
        if(cigar == CigarOperator.H) { 
            this.nextReadPos = 0;
            this.cigarElement = cigarIterator.next();
            this.cigarLength = cigarElement.getLength();
            this.cigar = cigarElement.getOperator();
        }
        else {
          this.nextReadPos = 0;
          this.cigarLength = rawCigarLength
        }
    }
   
    /**
     * Move forward one reference position.
     * If the position covers a deletion, this does not alter the
     * read position. If the position covers an insertion, it will
     * jump forward over the insertion in the read.
     */
    fun next() : Boolean {
        // TODO (next): implement this!
        
        ++cigarPos;
        if(cigarPos >= cigarLength) {
            if(!cigarIterator.hasNext())
                return false;
            cigarElement = cigarIterator.next();
            cigarPos = 0;
            cigarLength = cigarElement.getLength();
            cigar = cigarElement.getOperator();
        }
        
        when(cigar) {
            D -> {
                base = 0;
                qual = 0;
            }
            I -> {
                // Skip the insertion
                nextReadPos += cigarElement.getLength();
                
                if(!cigarIterator.hasNext())
                    return false;
                
                base = readBases[nextReadPos];
                qual = qualities[nextReadPos];
                cigarElement = cigarIterator.next();
                cigar = cigarElement.getOperator();
                cigarLength = cigarElement.getLength();
                
                // This is set to zero because we are returning the
                // first element of the next cigar block
                cigarPos = 0;
                ++nextReadPos;
            }
            H -> {
                // Skip the bases that were hard clipped - this should be happening
                // at the ends of reads (in theory)
                // In practise, we just have to skip this cigar element as if it 
                // never happened - the bases should have been physically removed
                cigarPos = 0;
                if(cigarIterator.hasNext()) {
                    System.err.println("Warning: CIGAR has more elements after hard clipping - unexpected");
                }
                base = 0;
                qual = 0;
                return false;
            }
            else -> {
                if(nextReadPos >= readBases.size) {
                    System.err.println("Cigar exceeded length of read " + this.read.getReadName() + " in cigar op " + cigar + " at cigar position " + cigarPos);
                    base = 0;
                }
                else {
                  base = readBases[nextReadPos];
                  qual = qualities[nextReadPos];
                }
                ++nextReadPos;
            }
        }
        
        return true;
    }
}

/**
 * Overall class containing pileup state
 * 
 * @author simon.sadedin@mcri.edu.au
 */
class PileupIterator(reader: SamReader, region: IRegion) : Iterator<Pileup> {
    
    val region  = region

//    val iter = reader.query(region.chr, region.from, region.to, false)
    
    var iteratorPosition : Int = -1
    
    val alignments = ArrayList<PileupState>()
    
    var nextRead : SAMRecord? = null
    
    var readIterator = reader.query(region.chr, region.from, region.to, false)
    
    val minMappingQuality = 1
    
    override fun hasNext(): Boolean {
        return iteratorPosition < this.region.to
    }

    override fun next(): Pileup {
        
        if(!hasNext()) {
            if(!this.alignments.isEmpty())
                this.alignments.clear()
        }
        else
            advance()
        
        return Pileup(this)
    }
    
    fun advance() {
        
        val start = region.from
         do {
          if(iteratorPosition != -1)
              ++iteratorPosition;
          
          var r = nextRead;
          while(true) {

              // Skip reads until we find a valid one
              while(r == null && this.readIterator.hasNext()) {
                  r = readIterator.next();
                  
                  // Ignore reads that are unmapped or mapped below the threshold set
                  if(r != null && (r.getReadUnmappedFlag() || r.getMappingQuality()<this.minMappingQuality))
                      r = null;
              }

              if(iteratorPosition == -1) {
                  
                  if(r != null) {
                    iteratorPosition = Math.min(r.getAlignmentStart(),start);
                  }
                  else { // No valid reads left
                      iteratorPosition = start;
                      break;
                  }
              }

              // Pile up all the reads at this position
              if(r != null && r.getAlignmentStart() == iteratorPosition) {
                  alignments.add(PileupState(r));
                  nextRead = null;
                  r = null;
                  continue;
              }
              else 
                  break;
          }

          // The next read is past the current position - we should return
          // First though, remove all the reads that do not overlap this position
          val iter = this.alignments.iterator();
          while(iter.hasNext()) {
              if(!iter.next().next())// next returns false if the position doesn't overlap
                  iter.remove();
          }
          
          this.nextRead = r;        
        } 
        while(iteratorPosition > 0 && iteratorPosition < start) ;

    }
}