package kg

import htsjdk.samtools.SAMRecord
import java.io.Serializable

/**
 * Interface allowing any arbitrary class to be a region
 */
interface IRegion {
	val chr : String
	val from : Int
	val to : Int
	
	val size get() = this.to - this.from
	
	companion object {
		val EMPTY_REGION  = Region("empty",0,0, null)
	}
		/**
	 * Return a region representing the region common to this region
	 * and the other region-like object
	 */
    fun intersect(other : IRegion) : IRegion {

        if(this.chr != other.chr)
            return EMPTY_REGION
            
        if(this == EMPTY_REGION)
            return EMPTY_REGION
           
        if(other == EMPTY_REGION)
            return EMPTY_REGION
            
        val ixFrom = Math.max(this.from, other.from)
        val ixTo = Math.min(this.to, other.to)
        if(ixTo<ixFrom)
            return EMPTY_REGION

        return Region(this.chr, ixFrom, ixTo, null)
    }
	
    /**
     * @return true iff the specified region partially or fully overlaps this one
     */
    fun overlaps(chr : String , from : Int, to : Int) : Boolean {
        if(chr != this.chr)
            return false

        val result =
			   (to < this.to && to > this.from) || 
               (from > this.from && from < this.to) ||
               (this.to >= from && this.to <= to)
        return result
    }
    
    /**
     * @return true iff the specified region partially or fully overlaps this one
     */
	fun overlaps(other : IRegion) : Boolean {
		return overlaps(other.chr, other.from, other.to)
	}

	/**
	 * @return true iff this region overlaps the given point
	 */
	fun overlaps(chr : String, pos: Int) : Boolean {
		return chr == this.chr && (this.from < pos) && (this.to > pos)
	}

	/**
	 * @return * true iff this region fully contains the other
	 */
	fun contains(other: IRegion) : Boolean {
        return chr == this.chr && (this.from < other.from) && (this.to > other.to)
	}
}


/**
 * Concrete representation of a genomic region
 */
open class Region<T> : IRegion, Serializable {
	
	override val chr  : String
	override val from  : Int
	override val to  : Int
	
	var data: T? = null

    companion object {
        operator fun invoke(read : SAMRecord) : Region<SAMRecord> {
			return Region(read.referenceName, read.alignmentStart, read.alignmentEnd, read)
		}
	}

//	constructor(read: SAMRecord) {
//        this.chr = read.referenceName
//		this.from = read.alignmentStart
//		this.to = read.alignmentEnd
//        this.data = read
//    }

	constructor(chr: String, from: Int, to: Int, data : T? = null) {
		this.chr = chr
		this.from = from
		this.to = to
		this.data = data
	}
	
	constructor(region : String) {
		
		val colonIndex = region.indexOf(":")
        if(colonIndex<0) {
			this.chr = region
			this.from = 0
			this.to = 0
			return
		}
		
		this.chr = region.substring(0, colonIndex)
		var dashIndex = region.indexOf("-")
		
		var from : Int = 0
		var to : Int = 0
		try {
			from = region.substring(colonIndex+1, dashIndex).toInt()
			to = region.substring(dashIndex+1).toInt()
		}
		catch(f: java.lang.NumberFormatException) {
			from = region.substring(colonIndex+1, dashIndex).replace(",","").toInt()
			to = region.substring(dashIndex+1).replace(",","").toInt()
		}
		this.from = from
		this.to = to
	}
	
    fun widen(bp : Int) : Region<T> {
		return widen(bp,bp)
	}
	
   /**
     * @param leftBp    number of base pairs to widen by on the left
     * @param rightBp    number of base pairs to widen by on the right
     * 
     * @return  a new Region object representing an interval bp wider than this one,
     *          with the lower boundary being limited to zero
     */
    fun widen(leftBp : Int, rightBp : Int) : Region<T> {
		return Region(this.chr, Math.max(this.from-leftBp,0), this.to+rightBp, data)
    }
	
   /**
     * Compute the fraction of mutual overlap of this region with the other.
     * <p>
     * This is defined as, the minimum fraction of either region's overlap
     * with the other.
     * 
     *               |   20   |
     * |----------------------|
     *           60
     *               |------------------------------|
     *                               80
     * mutual overlap = min(20/60, 20/80) = 0.25
     *                               
     * @param other
     * @return	fraction of mutual overlap
     */
    fun mutualOverlap(other : IRegion) : Double {
        
        val ixSize = intersect(other).size
        
        if(ixSize == 0) 
            return 0.0
        
        return Math.min(ixSize / size.toDouble(), ixSize / other.size.toDouble())
    }
    
    override fun toString() : String {
        return "$chr:$from-$to"
    }
}

class BasicRegion : Region<Any> {
     constructor(chr: String, from : Int, to : Int) :  super(chr, from, to)
     constructor(value: String) :  super(value)
}
