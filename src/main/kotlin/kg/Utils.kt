package kg

import java.text.NumberFormat
import java.time.Duration

class Utils {

    companion object {

        val humanNumberFormat = NumberFormat.getNumberInstance()
        init {
            humanNumberFormat.maximumFractionDigits=1
            humanNumberFormat.minimumFractionDigits=0
        }

        val percNumberFormat = NumberFormat.getNumberInstance()

        init {
            percNumberFormat.maximumFractionDigits=2
            percNumberFormat.minimumFractionDigits=0
        }

        /**
         * Convenience method to format a fraction in a reasonable way as a percentage
         * <p>
         * Note: includes the '%' symbol.
         */
        fun perc(fraction: Double) : String {
            return humanNumberFormat.format(100.0*fraction)+"%"
        }

        fun format(value : Duration) : String {
            var d = value
            val days = d.toDays();
            d = d.minusDays(days);
            val hours = d.toHours();
            d = d.minusHours(hours);
            val minutes = d.toMinutes();
            d = d.minusMinutes(minutes);
            val seconds = d.getSeconds() ;
            return (if(days == 0L) "" else days.toString() + " days") +
                (if(hours == 0L) "" else hours.toString()+ "h,") +
                (if(minutes ==  0L) "" else minutes.toString() + "m,") +
                (if(seconds == 0L) "" else seconds.toString() + "s,");
        }
    }
}