package kg

import htsjdk.samtools.*
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class Alignment {
    
    var reader : SamReader? = null
    
    val readerFactory : SamReaderFactory
    
    var source : () -> SamInputResource 

    constructor(path:String) {
        this.readerFactory = SamReaderFactory.makeDefault()
        this.source =  { SamInputResource.of(File(path).toPath()) }
    }
    
    val samples get() : List<String> {
        readerFactory.open(source()).use {
            return it.getFileHeader().getReadGroups().map {
                it.getSample()?:"UNKNOWN"
            }.distinct()
        }
    }
    
    fun createReader() : SamReader {
        return readerFactory.open(source())
    }
    
    fun <T> useReader(fn : (r: SamReader) -> T) : T {
        return readerFactory.open(source()).use {
            fn(it)
        }
    }

    fun <W> useIterator(fn : (r: Iterator<SAMRecord>) -> W) : W {
        return readerFactory.open(source()).use {
            val i = it.iterator()
            i.use {
                fn(i)
            }
        }
    }


    fun <W> useIterator(region : IRegion, fn : (r: Iterator<SAMRecord>) -> W) : W {
        return readerFactory.open(source()).use {
            val i = it.query(region.chr, region.from, region.to, false)
            i.use {
                fn(i)
            }
        }
    }

    /**
     * Create and use a SAMFileWriter based on the headers of this Alignment
     */
    fun <T> useWriter(outputFileName : String, sorted : Boolean = false, c : (SAMFileWriter) -> T) {
        return FileOutputStream(outputFileName).use {
            useWriter(it, sorted, c)
        }
    }

    fun <T> useWriter(out: OutputStream, sorted : Boolean = false, c : (SAMFileWriter) -> T) {
        useReader { reader ->
            val f = SAMFileWriterFactory()
            val header = reader.fileHeader
            val w = f.makeBAMWriter(header, sorted, out)
            try {
                c(w)
            }
            finally {
                w.close()
            }
        }
    }


    /**
     * Compute the total number of reads spanning the given position
     */
    fun coverageAt(chr: String, position: Int) : Int {
        return useReader {
            PileupIterator(it, BasicRegion(chr, position, position)).next().iterator.alignments.size
        }
    }
     
    fun query(region : IRegion, fn: (SAMRecord) -> Any) {
        useReader { r ->
            val i = r.query(region.chr, region.from, region.to, false)
            while(i.hasNext()) {
                fn(i.next())
            }
        }
    }
}