package kg

import htsjdk.samtools.SAMFileWriter
import java.io.Closeable
import java.util.*
import kotlin.Comparator

class OrderedPairWriter(val samWriter: SAMFileWriter, val progress: Progress? = null, val verbose : Boolean = false) : Closeable {

    val buffer = TreeSet<ReadPair>({ p1, p2 ->
        p1.r2.alignmentStart.compareTo(p2.r2.alignmentStart)?:p1.readName.compareTo(p2.readName)
    })

    var written = 0

    var currentReferenceIndex = -1

    /**
     * Adds an alignment to this pair writer
     */
    fun addAlignmentPair(pair : ReadPair) {

        if(pair.chimeric)
            return

        progress?.count()

        val r1ReferenceIndex = pair.r1ReferenceIndex
        if(currentReferenceIndex<0)
            currentReferenceIndex = r1ReferenceIndex
        else
        if(r1ReferenceIndex != currentReferenceIndex) {
            flushBuffer()
        }

       currentReferenceIndex = r1ReferenceIndex

        var bufferedPair = if(buffer.isEmpty()) null else buffer.first()
        while(bufferedPair != null && bufferedPair.r2.alignmentStart < pair.r1.alignmentStart) {
            bufferedPair = buffer.pollFirst()
            if(verbose)
                println("WW: write R2 $bufferedPair.r1.readName ($bufferedPair.r1.referenceName:$bufferedPair.r1.alignmentStart,$bufferedPair.r2.referenceName:$bufferedPair.r2.alignmentStart)")

            samWriter.addAlignment(bufferedPair.r2)
            ++written
            bufferedPair = if(buffer.isEmpty()) null else buffer.first()
        }

        if(verbose)
            println("WW: write R1 $pair.r1.readName ($pair.r1.referenceName:$pair.r1.alignmentStart,$pair.r2.alignmentStart)")

        samWriter.addAlignment(pair.r1)
        buffer.add(pair)
    }

    private fun flushBuffer() {
        assert(false)
    }

    override fun close() {
        flushBuffer()
        this.samWriter.close()
        progress?.end()
    }
}