package kg

import java.time.Duration
import java.time.LocalDateTime
import java.util.*
import java.util.logging.Logger

class Progress(
        val timeInterval : Int = 30000,
        val lineInterval : Int = 500,
        val withTime : Boolean = false,
        val withRate : Boolean = false,
        val prefix  : String? = null,
        val extra : (() -> String)? = null,
        val log : Logger? = null,

        /**
         * Optional total: if given, percentage progress can be shown
         */
        val total : Int = -1
    ) {

    var count = 0

    var lastPrintCount = 0

    var lastPrintTimeMs = System.currentTimeMillis()

    var startTimeMs = -1L

    lateinit var startDate : LocalDateTime

    var out = System.err

    fun count(c: ((Int) -> Unit)? = null) {

        if(startTimeMs < 0) {
            startTimeMs = System.currentTimeMillis()
            startDate = LocalDateTime.now()
        }

        if(count % lineInterval == 0) {

            val deltaMs = System.currentTimeMillis() - lastPrintTimeMs
            if(deltaMs > timeInterval) {
                if(c!=null)
                    c(count)
                else {
                    var rateInfo = if(withRate) {
                        val rate = 1000.0*(count-lastPrintCount)/(deltaMs+1)
                        String.format(" @ %.2f/s ", rate)
                    }
                    else
                        ""

                    var padding = 12
                    val totalInfo = if(total>=0) {
                        padding = total.toString().length + 7
                        " (" + Utils.perc(count/(1.0+total)) + ")"
                    }
                    else
                        ""

//                    var extraInfo = if(extra != null) extra!!()  else ""
                    var extraInfo = extra?.invoke()?:""

                    val progressLine = if(withTime)
                        Date().toString() + (if(prefix!=null)"\t$prefix" else "") + " :\t Processed $count" + rateInfo + extraInfo
                    else
                        (if(prefix != null) "\t$prefix :\t" else "") + "Processed ${(java.lang.String.valueOf(count) + totalInfo).padEnd(padding)}" + rateInfo + extraInfo

                    if(log != null)
                        log.info(progressLine)
                    else
                        out.println(progressLine)
                }
                lastPrintTimeMs = System.currentTimeMillis()
                lastPrintCount = count
            }
        }
        ++count
    }

    fun end() {
        if(count == 0) {
            if(log != null)
                log.info("0 records processed")
            else
                out.println("0 records processed")
            return
        }

        val extraInfo = if(extra != null) extra!!() else ""
        val endTime = System.currentTimeMillis()
        val deltaMs = (endTime-startTimeMs)+1
        val endDate = LocalDateTime.now()

        val timeDelta = Duration.between(startDate, endDate)

        val rate = 1000.0*((count)/((deltaMs+1).toDouble()))
        val rateInfo = String.format(" @ %.2f/s ", rate)

        val progressLine = "Processed ${java.lang.String.valueOf(count)} in ${Utils.format(timeDelta)} ${rateInfo} " + (if(extra != null)  " ($extraInfo)" else "")
        if(log != null)
            log.info(progressLine)
        else
            out.println(progressLine)
    }

}