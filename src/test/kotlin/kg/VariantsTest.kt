package kg

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VariantsTest {

    @Test
    fun `test basic parsing`() {

        val variants = Variants("src/test/data/allele_depth_test.vcf")
        variants.parse()

        println("There are ${variants.sites.size} variants")

        assert(variants.sites.firstEntry().value.size == 1)

        val v = variants.sites.firstEntry().value.variants[0]

        assert(v.pos == 117174424)
        assert(v.alts[0] == "A")
        assert(variants.sites[117174432]!!.variants[0].alts.size == 2)
        assert(variants.sites[117174432]!!.variants[0].alts[0] == "A")
        assert(variants.sites[117174432]!!.variants[0].alts[1] == "T")

        assert(variants.sites[117174432]!!.alt == "A")
        assert(variants[117174432]!!.alt == "A")
    }

    @Test
    fun `iteration`() {
        val variants = Variants("src/test/data/allele_depth_test.vcf").parse()

        assert(variants.count {
            it.alt == "A"
        } == 3)

        assert(variants.maxBy { it.pos }!!.pos == 117174452)
    }

    @Test
    fun `variant allele fraction`() {
        val variants = Variants("src/test/data/allele_depth_test.vcf").parse()

        println(variants[117174424]!!.variants[0].genotypes[0].altDepth )

        assert(variants[117174424]!!.variants[0].genotypes[0].altDepth == 46)
    }

    @Test
    fun `parsing samples`() {
        val variants = Variants("src/test/data/test.trio.vep.vcf").parse()

        assert(variants.samples.size == 3)

        assert(variants.samples[0] == "NA12877")
        assert(variants.samples[1] == "NA12878")
        assert(variants.samples[2] == "NA12879")
    }

    @Test
    fun `dosage by sample`() {
        val variants = Variants("src/test/data/test.trio.vep.vcf").parse()

        val site = variants[47359342]

        println(site!!.dosageOf("NA12878"))
        assert(site!!.dosageOf("NA12878") == 0)
        assert(site!!.dosageOf("NA12877") == 2)
    }
}