package kg

import htsjdk.samtools.SAMRecord
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OrderedPairWriterTest {

    @Test
    fun `test writing simple BAM file`() {
        val a = Alignment("src/test/data/small.test.bam")
        val reads = a.useIterator {
            it.asSequence().toList().shuffled()
        }

        a.useWriter("src/test/data/small.test.out.bam") { w ->
            val opw = OrderedPairWriter(w)
            reads.forEach {
                w.addAlignment(it)
            }
        }

        // TODO: index bam file?

        // assert => BAM file is sorted
        var lastRead : SAMRecord? = null
        Alignment("src/test/data/small.test.out.bam").useIterator {
            it.forEach {
                if(lastRead != null && it.alignmentStart<lastRead!!.alignmentStart && it.referenceIndex == lastRead!!.referenceIndex) {
                    println("Read $it at ${it.alignmentStart} out of order compared to ${lastRead} (${lastRead!!.alignmentStart})!")
                    assert(false, { "Reads are out of order"})
                }
                lastRead = it
            }

        }

    }

}