package kg

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.TestInstance

import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PileupIteratorTest {
    
    @Test
    fun `test simple pileup`() {
        
        val bam = Alignment("src/test/data/small.test.bam")
        
        bam.useReader {
            val pi = PileupIterator(it, BasicRegion("chrM", 2530, 2550))
            
            pi.forEach {
                println(it.position.toString() + ":\t" + it.summaryAsMap)
            }
            
        }
    }
   
}