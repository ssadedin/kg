package kg

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.Collections

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RangeIndexTest {

	@Test
    fun `Add simple range`() {
		val r = Region<Any>("chr1", 10, 100)
		
		val index = RangeIndex("chr1", listOf(r))
		
		assert(index.ranges[10]!!.contains(r))
	}
	
	@Test
    fun `Add two overlapping ranges`() {
		val r1 = Region<Any>("chr1", 10, 100)
		val r2 = Region<Any>("chr1", 50, 150)
		
		val index = RangeIndex("chr1",listOf(r1,r2))
		
		assert(index.ranges[10]!!.contains(r1))
		assert(index.ranges[50]!!.contains(r1))

		assert(index.ranges[151]!!.isEmpty())
	}
	
	
	@Test
    fun `Two overlapping ranges overlap`() {
		val index = RangeIndex.of("chr1", 10..100)
		println(index.getOverlaps(40,50,false))
	}
    
    @Test
    fun `simple intersect`() {
		val index = RangeIndex.of("chr1", 10..100, 50..150)
        val i = index.intersect(30,40)
        assert(!i.isEmpty())
        assert(i.size == 1)
        
        val j = index.intersect(80,90)
        assert(j.size == 2)
        
        val k = index.intersect(200,210)
        assert(k.isEmpty())
    }
    
    @Test
    fun `intersect two regions`() {
        val regions = RangeIndex.of("chr1",
            10..100,
            50..200
        )

        val index = RangeIndex("chr1",regions)
        val os = index.intersect(70,90)
        assert(os.size == 2)
        assert(os[0].from == 70)
        assert(os[1].from == 70)
    }
    
    @Test
    fun `iterate over ranges`() {
        val regions = RangeIndex.of("chr1", 
            10..100,
            50..200,
            300..400, // out of order: but iteration should be in order anyway
            50..250,
            500..600,
            550..600,
            601..610
        )
        
        val i = regions.iterator()
        assert(i.next().from == 10)
        assert(i.next().from == 50)
        assert(i.next().from == 50)
        assert(i.next().from == 300)
        assert(i.next().from == 500)
        assert(i.next().from == 550)
        assert(i.next().from == 601)
    }
    
    @Test
    fun `reduce simple`() {
        val regions = RangeIndex.of("chr1", 
            10..100,
            50..200
        )
        
        val reduced = regions.reduce()
        val i = reduced.iterator()
        
        val r1 = i.next()
        assert(r1.from == 10)
        assert(r1.to == 200)
    }
    
    @Test
    fun `reduce 3`() {
        val regions = RangeIndex.of("chr1", 
            10..100,
            70..90,
            50..200
        )
        
        val reduced = regions.reduce()
        val i = reduced.iterator()
        
        val r1 = i.next()
        assert(r1.from == 10)
        assert(r1.to == 200)
    }
 
    @Test
    fun `reduce with data`() {
        val regions = RangeIndex<Map<String,Int>>("chr1")
        regions.add(Region("chr1", 30, 70, mapOf("foo" to 1, "bar" to 5)))
        regions.add(Region("chr1", 10, 100))
       
        val reduced = regions.reduce()
        val i = reduced.iterator()
        
        val r1 = i.next()
        assert(r1.from == 10)
        assert(r1.to == 100)
        assert(r1.data?.get("foo") == 1)
        
    }

    @Test
    fun `reduce with conflicting data uses second value`() {
        val regions = RangeIndex<Map<String,Int>>("chr1")
        regions.add(Region("chr1", 30, 70, mapOf("foo" to 1, "bar" to 5)))
        regions.add(Region("chr1", 10, 100))

        // overlaps the first region but conflicts with its data
        regions.add(Region("chr1", 20, 25, mapOf("hey" to 10)))

        val reduced2 = regions.reduce()
        val r21 = reduced2.first()
        assert(r21.data?.get("hey") == 10)
        assert(r21.data?.get("foo") == null)
    }

    @Test
    fun `reduce with data and reducer`() {
        val regions = RangeIndex<Map<String,Int>>("chr1")
        regions.add(Region("chr1", 30, 70, mapOf("foo" to 1, "bar" to 5)))
        regions.add(Region("chr1", 10, 100, mapOf("foo" to 5)))
       
        val reduced = regions.reduce { r1, r2 ->
            
            val r1Data = if(r1.data != null) r1.data!! else Collections.emptyMap()
            val r2Data = if(r2.data != null) r2.data!! else Collections.emptyMap()

            val allKeys = r1Data.keys + r2Data.keys 

            allKeys.associate { key ->
                val total = (r1Data.get(key)?:0)  + (r2Data.get(key)?:0)
                key to total
            }
        }
        
        println(reduced.first().data)
        assert(reduced.first().data?.get("foo") == 6)
    }
    
    @Test
    fun `iterate empty index`() {
        val regions = RangeIndex<Map<String,Int>>("chr1")
        
        var i = 0
        regions.forEach {
            ++i
        }
        
        assert(i == 0)
    }
    
}