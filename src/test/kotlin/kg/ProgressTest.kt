package kg

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.ByteArrayOutputStream
import java.io.PrintStream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProgressTest {

    @Test
    fun `test basic progress`() {
        val p = Progress(timeInterval = 100)

        val bas = ByteArrayOutputStream()
        p.out = PrintStream(bas)
        for (i in 1..2000) {
            if(i % 100 == 0)
                Thread.sleep(50)
            p.count()
        }
        p.end()

        val result = bas.toString()
        println(result)

        assert(result.lines().any { it.contains("Processed 1500")})
        assert(result.lines().none { it.contains("null")})
        assert(result.lines().none { it.contains("%")})
    }

    @Test
    fun `test with extra`() {
        val p = Progress(timeInterval = 100, extra = {
            "boo boo head"
        })

        val bas = ByteArrayOutputStream()
        p.out = PrintStream(bas)
        for (i in 1..2000) {
            if(i % 100 == 0)
                Thread.sleep(50)
            p.count()
        }
        p.end()

        val result = bas.toString().trim()
        println(result)

        assert(result.lines().filter { !it.contains("in ")}.all { it.contains("boo boo head")})
    }

    @Test
    fun `test with percentage`() {
        val p = Progress(timeInterval = 100, total = 2000)

        val bas = ByteArrayOutputStream()
        p.out = PrintStream(bas)
        for (i in 1..2000) {
            if(i % 100 == 0)
                Thread.sleep(50)
            p.count()
        }
        p.end()

        val result = bas.toString().trim()
        println(result)
        assert(result.lines().filter { !it.contains(" in ")}.all {it.contains("%")})
    }
}