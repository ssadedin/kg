package kg

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RegionTest {

	@Test
    fun `default constructor works`() {
		println("I am testing that I can say hello: hello")
		
		val r = Region<Any>("chr1", 10, 100)
		
		println("R: ${r.chr}")
		
		assert(r.chr == "chr1")
		assert(r.from == 10)
		assert(r.to == 100)
		assert(r.size == 90)
	}
	
	@Test
	fun `parse region without commas`() {
		val r = Region<Any>("chr1:10-100")
		assert(r.chr == "chr1")
		assert(r.from == 10)
		assert(r.to == 100)
	}
	
	@Test
	fun `parse region with commas`() {
		val r = Region<Any>("chr1:100,000-100,100")
		assert(r.chr == "chr1")
		assert(r.from == 100000)
		assert(r.to == 100100)
	}

	@Test
	fun `test widen`() {
		val r = Region<Any>("chr1:50-100")
		val w = r.widen(20,20)
		assert(w.from == 30)
		assert(w.to == 120)
	}
	
	@Test
	fun `test overlap`() {
		var r1 = Region<Any>("chr1:50-100")
		var r2 = Region<Any>("chr1:50-100")
		assert(r1.overlaps(r2))
		
		r2 =  Region<Any>("chr1:50-51")
		assert(r1.overlaps(r2))
		assert(r2.overlaps(r1))
		
		r2 =  Region<Any>("chr2:50-51")
		assert(!r1.overlaps(r2))
		assert(!r2.overlaps(r1))

	}
	
	@Test
	fun `intersect`() {
		var r1 = Region<Any>("chr1:50-100")
		var r2 = Region<Any>("chr1:80-90")
		
		var r = r1.intersect(r2)
		assert(r.from == 80)
		assert(r.to == 90)
		
		r1 = Region<Any>("chr1:50-100")
		r2 = Region<Any>("chr1:180-190")
		r = r1.intersect(r2)
		assert(r.size == 0)
		assert(r == IRegion.EMPTY_REGION)
		
		r1 = Region<Any>("chr1:50-100")
		r2 = Region<Any>("chr1:90-110")
		r = r1.intersect(r2)
		assert(r.from == 90)
		assert(r.to == 100)
	}

}