package kg

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.TestInstance

import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RegionsTest {
    
    @Test
    fun `test iterate regions`() {
        val regions = Regions<String>()
        
        listOf(
           Region("chr1", 10, 50, "cat"),
           Region("chr2", 30, 90, "dog"),
           Region("chr2", 70, 190, "tree")
        ).forEach {
            regions.add(it)
        }
        
        val i = regions.iterator()
        
        assert(i.next().data == "cat")
        assert(i.next().data == "dog")
        assert(i.next().data == "tree")
        
    }
    
    @Test
    fun `iterate empty regions`() {
        val regions = Regions<String>()
        
        var i = 0
        regions.forEach {
            ++i
        }
        
        assert(i == 0)
    }
    
    @Test
    fun `read bed file`() {
        
        
        val f = File(".")
        
        println("Dir = ${f.absolutePath}")
        
        val bed = Regions.bed("src/test/data/test.bed")
        
        assert(bed.count() == 280)
        assert(bed.size == 35778)
        
        println("Number of regions is ${bed.count()}")
    }
    
    @Test
    fun `intersect regions`() {
        
        val r1 = Regions(listOf(
           Region("chr1", 10, 50, "cat"),
           Region("chr2", 30, 90, "dog"),
           Region("chr2", 70, 190, "tree")
        ))
        
        val r2 = Regions(listOf(
           Region("chr1", 40, 45, "cat"),
           Region("chr2", 80, 90, "tree")
        ))
        
        /*
           chr1:
 
           10                    50
           |---------------------|

            ==================================================================

                       40    45
                       |-----|

          chr2:
 
                 30                             90
                 |------------------------------|
                                        70                             190
                                        |------------------------------|

            ==================================================================

                                            80  90
                                            |---|
        */
         
        val ix = r1.intersect(r2)
        
        assert(ix.numberOfRanges == 3)
        
        val i = ix.iterator()
        assert(i.next().from == 40)
        assert(i.next().from == 80)
        assert(i.next().from == 80)

        
        val i2 = ix.iterator()
        assert(i2.next().to == 45)
        assert(i2.next().to == 90)
        assert(i2.next().to == 90)
    }
    
//    @Test
//    fun `reduce large`() {
//        val bed = Regions.bed("/Users/simon.sadedin/work/cpipe/designs/SSQXTCRE1_REFFLAT_38/SSQXTCRE1_REFFLAT_38.bed")
//        
//        val ms = kotlin.system.measureTimeMillis {
//            for(i in 1..10) {
//                val x = bed.reduce()
//                println("Reduced size is ${x.size}")
//            }
//        }
//        println("Finished in ${ms}ms")
//    }
}