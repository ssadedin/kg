package kg

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.TestInstance

import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AlignmentTest {
    
    @Test
    fun `test get samples`() {
        
        val bam = Alignment("src/test/data/small.test.bam")
        
        assert(bam.samples.size == 1, { "Incorrect number of samples: ${bam.samples.size}"})
        assert(bam.samples[0] == "NA12878")
    }
    
    @Test
    fun `query reads in region`() {
        
        val bam = Alignment("src/test/data/small.test.bam")
        
        val numReads = bam.useIterator(BasicRegion("chr1:14788-20000")) {
            var n = 0
            it.forEach {
                println(it.readName)
                ++n
            }
            n
        }
        assert(numReads > 10)
    }
    
    @Test
    fun `coverage at position`() {
        val bam = Alignment("src/test/data/small.test.bam")
        assert(bam.coverageAt("chrM", 2540) == 11)

        assert(bam.coverageAt("chrM", 8540) == 0)

    }
    
}