# kg - Kotlin Genomics Library

A Kotlin library for working with Next Generation Sequencing data

This project at the moment is mainly just for fun and as a way for me to learn
a bit of Kotlin. It is mostly so far just directly porting over the functions 
and classes from Groovy NGS Utils (https://github.com/ssadedin/groovy-ngs-utils).
